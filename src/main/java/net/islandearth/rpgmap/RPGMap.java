package net.islandearth.rpgmap;

import co.aikar.commands.PaperCommandManager;
import co.aikar.idb.DB;
import net.islandearth.languagy.language.Language;
import net.islandearth.languagy.language.LanguagyImplementation;
import net.islandearth.languagy.language.LanguagyPluginHook;
import net.islandearth.languagy.language.Translator;
import net.islandearth.rpgmap.commands.RPGMapCommand;
import net.islandearth.rpgmap.listener.ConnectionListener;
import net.islandearth.rpgmap.managers.RPGMapManagers;
import net.islandearth.rpgmap.map.MapLocation;
import net.islandearth.rpgmap.map.MapUpdateTask;
import net.islandearth.rpgmap.map.render.RPGRenderer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.map.MapCursor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class RPGMap extends JavaPlugin implements LanguagyPluginHook {

	public RPGMapManagers getManagers() {
		return managers;
	}

	private RPGMapManagers managers;

	public MapUpdateTask getMapUpdateTask() {
		return mapUpdateTask;
	}

	private MapUpdateTask mapUpdateTask;

	public Translator getTranslator() {
		return translator;
	}

	@LanguagyImplementation(fallbackFile = "plugins/RPGMap/lang/en_gb.yml")
	private Translator translator;

	@Override
	public void onEnable() {
		this.createConfig();
		this.managers = new RPGMapManagers(this);

		File renderersFile = new File(getDataFolder() + "/renderers/");
		if (!renderersFile.exists()) {
			renderersFile.mkdirs();
		}

		File mapFile = new File(getDataFolder() + "/maps/");
		if (!mapFile.exists()) {
			mapFile.mkdirs();
		}

		for (File file : mapFile.listFiles()) {
			getLogger().info("Loading MapLocation: " + file.getName());
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			if (Bukkit.getWorld(UUID.fromString(config.getString("world"))) == null) getLogger().severe("Cannot find world (null)!");
			this.getManagers().getMapCache().getMapLocations().add(new MapLocation(config.getInt("x"),
					config.getInt("y"),
					config.getInt("z"),
					Bukkit.getWorld(UUID.fromString(config.getString("world"))),
					MapCursor.Type.valueOf(config.getString("type"))));
		}

		if (mapFile.listFiles().length < this.getManagers().getMapCache().getMapLocations().size()) {
			getLogger().warning("Some MapLocation's could not be loaded!");
		}

		this.generateLang();
		this.registerRenders();
		this.registerListeners();
		this.registerCommands();
	}

	@Override
	public void onDisable() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			this.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
		});
		DB.close();
	}

	private void generateLang() {
		File lang = new File(this.getDataFolder() + "/lang/");
		if (!lang.exists()) lang.mkdirs();

		for (Language language : Language.values()) {
			File file = new File(getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			config.options().copyDefaults(true);
			config.addDefault("toggled", "&aYour map has been toggled.");
			config.addDefault("invalid_cursor", "&cInvalid cursor type.");
			config.addDefault("location_saved", "&aMap location saved.");
			config.addDefault("cursor_removed", "&aThe cursor has been removed.");
			config.addDefault("location_not_saved", "&cYour current location is not saved as a cursor.");

			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void createConfig() {
		FileConfiguration config = this.getConfig();
		String header;
		String eol = System.getProperty("line.separator");
		header = "This is the global config for RPGMap." + eol;
		header += "OPTIONS: " + eol;
		header += "features.renderers : all renderers on the map displayed to the player, in priority order. The map will use the first renders scale (highest priority)." + eol;
		config.options().header(header);
		config.addDefault("storage", "file");
		config.addDefault("sql.host", "localhost");
		config.addDefault("sql.port", 3306);
		config.addDefault("sql.db", "RPGMap");
		config.addDefault("sql.user", "user");
		config.addDefault("sql.pass", "pass");
		config.addDefault("features.torch_swap", true);
		Bukkit.getWorlds().forEach(world -> config.addDefault(world.getName() + ".features.renderers", Arrays.asList("RPGRenderer")));
		config.options().copyDefaults(true);
		saveConfig();
	}

	private void registerRenders() {
		RPGRenderer renderer = new RPGRenderer(this);
		this.getManagers().getMapRegistry().register(renderer);
	}

	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new ConnectionListener(this), this);
	}

	private void registerCommands() {
		PaperCommandManager manager = new PaperCommandManager(this);
		manager.registerCommand(new RPGMapCommand(this));
		manager.getCommandCompletions().registerAsyncCompletion("cursors", c -> {
			List<String> cursors = new ArrayList<>();
			for (MapCursor.Type cursor : MapCursor.Type.values()) {
				cursors.add(cursor.toString());
			}
			return cursors;
		});
	}

	@Override
	public void onLanguagyHook() {
		translator.setDisplay(Material.MAP);
	}
}
