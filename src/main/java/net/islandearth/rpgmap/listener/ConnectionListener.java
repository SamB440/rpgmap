package net.islandearth.rpgmap.listener;

import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.map.MapUpdateTask;
import net.islandearth.rpgmap.map.render.Render;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

public class ConnectionListener implements Listener {

	private Map<UUID, Integer> playerSchedulers = new HashMap<>();
	private final RPGMap plugin;
	private final Map<String, List<Render>> renders;

	public ConnectionListener(RPGMap plugin) {
		this.plugin = plugin;
		this.renders = new HashMap<>();
		Bukkit.getWorlds().forEach(world -> {
			List<Render> renderList = new ArrayList<>();
			plugin.getConfig().getStringList(world.getName() + ".features.renderers").forEach(renderName -> {
				Render render = plugin.getManagers().getMapRegistry().getRender(renderName);
				renderList.add(render);
			});

			renders.put(world.getName(), renderList);
		});
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent pje) {
		Player player = pje.getPlayer();
		MapUpdateTask task;
		if (renders.get(player.getWorld().getName()) != null && !renders.get(player.getWorld().getName()).isEmpty()) {
			playerSchedulers.put(player.getUniqueId(), Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, task = new MapUpdateTask(plugin,
					renders.get(player.getWorld().getName()),
					player.getUniqueId()), 20L, 20L));
			task.updateMap();
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent pqe) {
		Player player = pqe.getPlayer();
		if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
			plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
		if (playerSchedulers.containsKey(player.getUniqueId()))
			Bukkit.getScheduler().cancelTask(playerSchedulers.get(player.getUniqueId()));
	}

	@EventHandler
	public void onWorldSwap(PlayerChangedWorldEvent pcwe) {
		Player player = pcwe.getPlayer();
		if (playerSchedulers.containsKey(player.getUniqueId())) Bukkit.getScheduler().cancelTask(playerSchedulers.get(player.getUniqueId()));

		MapUpdateTask task;
		if (renders.get(player.getWorld().getName()) != null && !renders.get(player.getWorld().getName()).isEmpty()) {
			playerSchedulers.put(player.getUniqueId(), Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, task = new MapUpdateTask(plugin,
					renders.get(player.getWorld().getName()),
					player.getUniqueId()), 20L, 20L));
			task.updateMap();
		}
	}
}
