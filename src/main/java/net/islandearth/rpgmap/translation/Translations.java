package net.islandearth.rpgmap.translation;

import net.islandearth.rpgmap.RPGMap;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public enum Translations {
	TOGGLED,
	INVALID_CURSOR,
	LOCATION_SAVED,
	CURSOR_REMOVED,
	LOCATION_NOT_SAVED;

	private String getPath() {
		return this.toString().toLowerCase();
	}

	public void send(Player player) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		String message = plugin.getTranslator().getTranslationFor(player, this.getPath());
		player.sendMessage(message);
	}

	public void send(Player player, String... values) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		String message = plugin.getTranslator().getTranslationFor(player, this.getPath());
		for (int i = 0; i < 10; i++) {
			if (values.length > i) message = message.replaceAll("%" + i, values[i]);
			else break;
		}

		player.sendMessage(message);
	}

	public void sendList(Player player) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		List<String> message = plugin.getTranslator().getTranslationListFor(player, this.getPath());
		message.forEach(player::sendMessage);
	}

	public void sendList(Player player, String... values) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		List<String> messages = plugin.getTranslator().getTranslationListFor(player, this.getPath());
		messages.forEach(message -> {
			for (int i = 0; i < 10; i++) {
				if (values.length > i) message = message.replaceAll("%" + i, values[i]);
				else break;
			}

			player.sendMessage(message);
		});
	}

	public String get(Player player) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		return plugin.getTranslator().getTranslationFor(player, this.getPath());
	}

	public List<String> getList(Player player) {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		return plugin.getTranslator().getTranslationListFor(player, this.getPath());
	}
}
