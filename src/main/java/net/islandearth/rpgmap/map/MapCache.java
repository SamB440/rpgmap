package net.islandearth.rpgmap.map;

import java.util.ArrayList;
import java.util.List;

public class MapCache {

	private List<MapLocation> mapLocations = new ArrayList<>();

	public List<MapLocation> getMapLocations() {
		return mapLocations;
	}

}
