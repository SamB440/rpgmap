package net.islandearth.rpgmap.map;

import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.map.render.Render;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import java.util.*;

public class MapUpdateTask implements Runnable, Listener {

	private final UUID uuid;
	private final RPGMap plugin;
	private final List<Render> renders;
	private final List<String> blacklistedWorlds;
	
	public MapUpdateTask(RPGMap plugin, List<Render> renders, UUID uuid) {
		this.plugin = plugin;
		this.renders = renders;
		this.uuid = uuid;
		if (renders == null || renders.isEmpty()) throw new IllegalArgumentException("Renderers is empty or null !");
		Bukkit.getPluginManager().registerEvents(this, plugin);
		this.blacklistedWorlds = new ArrayList<>();
		renders.forEach(render -> {
			blacklistedWorlds.addAll(render.getConfig().getStringList("ignoredMaps"));
		});
	}
	
	public List<Render> getRenders() {
		return renders;
	}
	
	@Override
	public void run() {
		Player player = Bukkit.getPlayer(uuid);
		if (player != null && !player.isDead() && player.isValid() && !player.isSleeping()) {
			plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).whenComplete((account, error) -> {
				if (account.isMapEnabled() && !blacklistedWorlds.contains(player.getWorld().getName())) {
					if (!isRPGMap(player.getInventory().getItemInOffHand()) && player.getEyeLocation().getBlock().getLightFromSky() > 0 && player.getEyeLocation().getBlock().getType() != Material.CAVE_AIR) {
						Bukkit.getScheduler().runTask(plugin, this::updateMap);
					} else if (plugin.getConfig().getBoolean("features.torch_swap")) {
						if (player.getEyeLocation().getBlock().getLightFromSky() == 0 && player.getEyeLocation().getBlock().getType() == Material.CAVE_AIR) {
							player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));

							if (player.getInventory().contains(Material.TORCH)) {
								for (int slot = 0; slot < player.getInventory().getSize(); slot++) {
									ItemStack item = player.getInventory().getItem(slot);
									if (item != null) {
										if (item.getType() == Material.TORCH) {
											player.getInventory().setItemInOffHand(item);
											player.getInventory().setItem(slot, new ItemStack(Material.AIR));
											break;
										}
									}
								}
							}
						}
					}
				} else {
					if (player.getInventory().getItemInOffHand().getType() == Material.FILLED_MAP) {
						if (isRPGMap(player.getInventory().getItemInOffHand())) {
							player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
						} else {
							Map<Integer, ItemStack> notAdded = player.getInventory().addItem(player.getInventory().getItemInOffHand());
							for (ItemStack item : notAdded.values()) {
								player.getWorld().dropItem(player.getEyeLocation(), item).setVelocity(player.getEyeLocation().getDirection().multiply(1.5));
							}
							player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
						}
					}
				}
			});
		}
	}
	
	public void updateMap() {
		Player player = Bukkit.getPlayer(uuid);
		MapView map = Bukkit.createMap(player.getWorld());
		map.setScale(renders.get(0).getScale());
		map.setUnlimitedTracking(false);
		map.getRenderers().clear();
		renders.forEach(render -> map.addRenderer((MapRenderer) render));
		map.setCenterX(player.getLocation().getBlockX());
		map.setCenterZ(player.getLocation().getBlockZ());

		ItemStack item = new ItemStack(Material.FILLED_MAP);
		MapMeta im = (MapMeta) item.getItemMeta();
		im.setLore(Arrays.asList(ChatColor.WHITE + "World Map",
				ChatColor.GRAY + "Type /rpgmap to toggle."));
		im.setMapView(map);
		item.setItemMeta(im);

		ItemStack offhand = player.getInventory().getItemInOffHand();
		if (offhand.getType() != Material.AIR && !isRPGMap(player.getInventory().getItemInOffHand())) {
			if (offhand.getType() != Material.SHIELD) {
				Map<Integer, ItemStack> notAdded = player.getInventory().addItem(offhand);
				for (ItemStack items : notAdded.values()) {
					if (items.getType() != Material.AIR) player.getWorld().dropItem(player.getEyeLocation(), items).setVelocity(player.getEyeLocation().getDirection().multiply(1.5));
				}
			}
			player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
		}
		player.getInventory().setItemInOffHand(item);
	}
	
	private boolean isRPGMap(ItemStack item) {
		return item != null && item.getType() == Material.FILLED_MAP && item.hasItemMeta() && item.getItemMeta().getLore().equals(Arrays.asList(ChatColor.WHITE + "World Map",
				ChatColor.GRAY + "Type /rpgmap to toggle."));
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent pte) {
		Player player = pte.getPlayer();
		// This is to prevent the map rendering wrongly
		if (this.isRPGMap(player.getInventory().getItemInOffHand())) {
			player.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
		}
	}
	
	@EventHandler
	public void onSwap(PlayerSwapHandItemsEvent e) {
		if (isRPGMap(e.getMainHandItem())) e.setCancelled(true);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent ice) {
		if (isRPGMap(ice.getCurrentItem())) ice.setCancelled(true);
	}
}
