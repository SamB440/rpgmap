package net.islandearth.rpgmap.map.render;

import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.map.MapLocation;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.map.*;
import org.bukkit.map.MapCursor.Type;
import org.bukkit.map.MapView.Scale;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class StandardRPGMap extends MapRenderer implements Render {

	private List<MapOption> mapOptions;
	private final RPGMap plugin;
	
	StandardRPGMap(RPGMap plugin) {
		this.plugin = plugin;
		this.reloadOptions();
	}
	
	@Override
	public void render(MapView map, MapCanvas canvas, Player player) {
		map.setCenterX(player.getLocation().getBlockX());
		map.setCenterZ(player.getLocation().getBlockZ());
		MapCursorCollection cursors = new MapCursorCollection();
		drawDefaultCursors(cursors, player);

		List<MapOption> mapOptions = getMapOptions();
		if (mapOptions.contains(MapOption.DRAW_COMPASS)) {
			canvas.drawText(0, 63, MinecraftFont.Font, "W");
			canvas.drawText(123, 63, MinecraftFont.Font, "E");
			canvas.drawText(63, 0, MinecraftFont.Font, "N");
			canvas.drawText(63, 121, MinecraftFont.Font, "S");
		}

		if (mapOptions.contains(MapOption.TRUE_COLOUR)) {
			//TODO
		}
		canvas.setCursors(cursors);
	}
	
	void drawDefaultCursors(MapCursorCollection cursors, Player player) {
		if (this.getMapOptions().contains(MapOption.DRAW_PLAYER)) cursors.addCursor(new MapCursor((byte) 0, (byte) 0, getDirection(player.getLocation().getYaw()), Type.valueOf(this.getConfig().getString("selfPointer")), true));
		
		for (MapLocation location : plugin.getManagers().getMapCache().getMapLocations()) {
			if (location.getWorld() == null) continue;
			if (!player.getLocation().getWorld().equals(location.getWorld())) continue;
			Location bukkitLocation = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
			if (player.getLocation().getWorld().getName().equals(bukkitLocation.getWorld().getName())) {
				if (player.getLocation().distance(bukkitLocation) <= this.getConfig().getDouble("renderDistance")) {
					int[] coords = calculateLocation(player, location.getX(), location.getZ());
					cursors.addCursor(new MapCursor((byte) coords[0], (byte) coords[1], (byte) 12, location.getCursorType(), true));
				}
			}
		}
	}
	
	/**
	 * Calculates a minecraft world position to a map position.
	 * @param player - the player
	 * @param worldX - the object location on the x axis
	 * @param worldZ - the object location on the z axis
	 * @return an int array with the map X and Y
	 */
	int[] calculateLocation(Player player, int worldX, int worldZ) {
		int playerX = player.getLocation().getBlockX();
		int playerZ = player.getLocation().getBlockZ();
		
		int mapX = 0;
		int mapY = 0;
		
		int difX = Math.abs(worldX - playerX);
		int difZ = Math.abs(worldZ - playerZ);
				
		if (worldX > playerX) {
			mapX = mapX + difX;
		} else {
			mapX = mapX - difX;
		}
		
		if (worldZ > playerZ) {
			mapY = mapY + difZ;
		} else {
			mapY = mapY - difZ;
		}

		return new int[]{mapX, mapY};
	}

	int[] calculateWorldLocation(Player player, int playerX, int playerZ) {
		int worldX = player.getLocation().getBlockX();
		int worldZ = player.getLocation().getBlockZ();

		int mapX = 0;
		int mapY = 0;

		int difX = Math.abs(worldX - playerX);
		int difZ = Math.abs(worldZ - playerZ);

		if (worldX > playerX) {
			mapX = mapX + difX;
		} else {
			mapX = mapX - difX;
		}

		if (worldZ > playerZ) {
			mapY = mapY + difZ;
		} else {
			mapY = mapY - difZ;
		}

		return new int[]{mapX, mapY};
	}

	/**
	 * @param yaw - player's yaw
	 * @return byte for map cursor, ranging from 0-15.
	 */
	static byte getDirection(double yaw) {
		return (byte) Math.min(15, Math.max(0, (((yaw + 371.25) % 360) / 22.5)));
	}
	

	@Override
	public FileConfiguration getConfig() {
		return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + "/renderers/" + this.getId() + ".yml"));
	}
	
	@Override
	public File getFile() {
		return new File(plugin.getDataFolder() + "/renderers/" + this.getId() + ".yml");
	}
	

	@Override
	public List<MapOption> getMapOptions() {
		return mapOptions;
	}

	@Override
	public void reloadOptions() {
		FileConfiguration config = this.getConfig();
		List<MapOption> options = new ArrayList<>();
		config.getStringList("options").forEach(value -> options.add(MapOption.valueOf(value.toUpperCase())));
		this.mapOptions = options;
	}
	
	@Override
	public void defaultConfig() throws IOException {
		FileConfiguration config = this.getConfig();
		config.options().copyDefaults(true);
		config.addDefault("options", Arrays.asList(MapOption.DRAW_COMPASS.toString(), MapOption.DRAW_PLAYER.toString()));
		config.addDefault("renderDistance", 100.0);
		config.addDefault("selfPointer", Type.GREEN_POINTER.toString());
		config.addDefault("scale", "CLOSE");
		config.save(this.getFile());
	}

	@Override
	public Scale getScale() {
		return Scale.valueOf(this.getConfig().getString("scale").toUpperCase());
	}
}