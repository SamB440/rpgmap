package net.islandearth.rpgmap.map.render;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MapRegistry {
	
	private final Map<String, Render> renders = new HashMap<>();
	
	public void register(Render clazz) {
		renders.put(clazz.getId(), clazz);
		File file = clazz.getFile();
		if (!file.exists()) {
			try {
				file.createNewFile();
				clazz.defaultConfig();
				clazz.reloadOptions();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Render getRender(String id) {
		return renders.get(id);
	}
}
