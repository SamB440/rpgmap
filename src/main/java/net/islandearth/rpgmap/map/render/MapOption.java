package net.islandearth.rpgmap.map.render;

public enum MapOption {
	DRAW_COMPASS,
	DRAW_PLAYER,
	TRUE_COLOUR
}
