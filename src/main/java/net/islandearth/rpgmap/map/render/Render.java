package net.islandearth.rpgmap.map.render;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.map.MapView;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Render {

	FileConfiguration getConfig();
	
	File getFile();
	
	String getId();
	
	List<MapOption> getMapOptions();

	MapView.Scale getScale();

	void reloadOptions();
	
	void defaultConfig() throws IOException;
}
