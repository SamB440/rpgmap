package net.islandearth.rpgmap.map.render;

import net.islandearth.rpgmap.RPGMap;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapCursor;
import org.bukkit.map.MapCursor.Type;
import org.bukkit.map.MapCursorCollection;
import org.bukkit.map.MapView;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RPGRenderer extends StandardRPGMap {

	private final RPGMap plugin;
	private final List<EntityType> ignored;
	
	public RPGRenderer(RPGMap plugin) {
		super(plugin);
		this.ignored = this.getIgnoredEntities();
		this.plugin = plugin;
	}
	
	@Override
	public void render(MapView map, MapCanvas canvas, Player player) {
		super.render(map, canvas, player);
		if (getConfig().getBoolean("fogOfWar")) map.setLocked(true);
	}
	
	@Override
	public void drawDefaultCursors(MapCursorCollection cursors, Player player) {
		super.drawDefaultCursors(cursors, player);
		
		// Entities
		int entityRenderDistance = getConfig().getInt("entityRenderDistance");
		for (Entity entity : player.getNearbyEntities(entityRenderDistance, entityRenderDistance, entityRenderDistance)) {
			if (this.getConfig().getBoolean("ignoreAll"))
				continue;

			Player playerEntity = null;
			if (entity instanceof Player) playerEntity = (Player) entity;
			// Calculate where to display the cursor on the map
			int[] coords = calculateLocation(player, entity.getLocation().getBlockX(), entity.getLocation().getBlockZ());

			if (playerEntity != null) {
				boolean isTeam = false;
				for (Team team : playerEntity.getScoreboard().getTeams()) {
					for (Team team2 : player.getScoreboard().getTeams()) {
						if (team.getName().equals(team2.getName())) {
							isTeam = true;
							break;
						}
					}
				}

				// Check team, if same team display
				if (isTeam) {
					cursors.addCursor(new MapCursor((byte) coords[0], (byte) coords[1], (byte) 12, Type.valueOf(this.getConfig().getString("teamPointer")), true));
					continue;
				}
			}
				
			// Check for ignored entities, and ignore the vehicle the player is in
			if (ignored.contains(entity.getType())) continue;
			if (player.getVehicle() != null && player.getVehicle().equals(entity)) continue;
			
			// Check if they're above ground
			boolean aboveGround = player.getLocation().getBlock().getType() != Material.CAVE_AIR;
			// Do not display entities below ground if player is above ground and config option is TRUE
			if (this.getConfig().getBoolean("ignoreCaveEntities") && entity.getLocation().getBlock().getType() == Material.CAVE_AIR && aboveGround) continue;
			
			// Find valid cursor
			Type type = null;
			if (this.getConfig().getString(entity.getType().toString()) != null) type = Type.valueOf(this.getConfig().getString(entity.getType().toString()));
			else type = Type.valueOf(this.getConfig().getString("entityPointer").toUpperCase());
			
			// Ignore invisibility
			if (this.getConfig().getBoolean("ignoreInvisibility") && entity instanceof LivingEntity && ((LivingEntity) entity).hasPotionEffect(PotionEffectType.INVISIBILITY)) return;

			if (entity instanceof Player) {
				Player npc = (Player) entity;
				if (npc.hasMetadata("NPC")) {
					cursors.addCursor(new MapCursor((byte) coords[0], (byte) coords[1], getDirection(entity.getLocation().getYaw()), Type.valueOf(this.getConfig().getString("npcPointer")), true));

					//TODO quests NPC support
				} else {
					cursors.addCursor(new MapCursor((byte) coords[0], (byte) coords[1], getDirection(entity.getLocation().getYaw()), type, true));
				}
			} else {
				cursors.addCursor(new MapCursor((byte) coords[0], (byte) coords[1], (byte) 12, type, true));
			}
		}
	}

	@Override
	public String getId() {
		return "RPGRenderer";
	}
	
	@Override
	public void defaultConfig() throws IOException {
		super.defaultConfig();
		FileConfiguration config = this.getConfig();
		config.options().copyDefaults(true);
		
		String header;
		String eol = System.getProperty("line.separator");
		header = "This is the default config for the " + this.getId() + " renderer." + eol;
		header += "OPTIONS: " + eol;
		header += "ignoreAll : whether to ignore all entities or not" + eol;
		header += "ignoredEntities : entities the renderer will ignore" + eol;
		header += "npcPointer : the cursor type to display for an NPC (Citizens)" + eol;
		header += "entityPointer : default value for entities if not specified" + eol;
		header += "ignoreCaveEntities : whether to ignore entities that are underground or not (only applies when player is above ground)" + eol;
		header += "ignoreInvisibility : whether or not to ignore invisibility" + eol;
		header += "teamPointer : pointer to display for own team players" + eol;
		header += "trueColour (Coming soon) : will make a best attempt to use proper colours for blocks" + eol;
		header += "ignoredMaps : list of worlds to ignore for this render" + eol;
		header += "entityRenderDistance : how far entities should render on the map. Higher values cause more lag!" + eol;
		header += "Note: You can set any entities cursor by just having the key as the entity name, and the value as the cursor type." + eol;
		header += "Entity types viewable at: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html" + eol;
		header += "Cursor types viewable at: https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/map/MapCursor.Type.html" + eol;
		config.options().header(header);
		config.addDefault("ignoreAll", true);
		config.addDefault("ignoredEntities", Collections.singletonList(EntityType.ARMOR_STAND.toString()));
		config.addDefault("npcPointer", Type.GREEN_POINTER.toString());
		config.addDefault(EntityType.PLAYER.toString(), Type.WHITE_POINTER.toString());
		config.addDefault("entityPointer", Type.SMALL_WHITE_CIRCLE.toString());
		config.addDefault(EntityType.VILLAGER.toString(), Type.SMALL_WHITE_CIRCLE.toString());
		config.addDefault("ignoreCaveEntities", true);
		config.addDefault("ignoreInvisibility", true);
		config.addDefault("teamPointer", Type.BLUE_POINTER.toString());
		config.addDefault("ignoredMaps", Collections.singletonList("aIgnoredWorld"));
		config.addDefault("entityRenderDistance", 30);
		config.save(this.getFile());
	}
	
	private List<EntityType> getIgnoredEntities() {
		List<EntityType> ignored = new ArrayList<>();
		this.getConfig().getStringList("ignoredEntities").forEach(value -> ignored.add(EntityType.valueOf(value.toUpperCase())));
		return ignored;
	}
}
