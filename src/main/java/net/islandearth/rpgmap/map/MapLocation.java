package net.islandearth.rpgmap.map;

import org.bukkit.World;
import org.bukkit.map.MapCursor;

public class MapLocation {
	
	private final int x;
	private final int y;
	private final int z;
	private final World world;
	private final MapCursor.Type cursorType;
	
	public MapLocation(int x, int y, int z, World world, MapCursor.Type cursorType) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.world = world;
		this.cursorType = cursorType;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public World getWorld() {
		return world;
	}
	
	public MapCursor.Type getCursorType() {
		return cursorType;
	}
}
