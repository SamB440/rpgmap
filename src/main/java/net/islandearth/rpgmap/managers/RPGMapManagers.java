package net.islandearth.rpgmap.managers;

import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.managers.data.StorageManager;
import net.islandearth.rpgmap.managers.data.StorageType;
import net.islandearth.rpgmap.map.MapCache;
import net.islandearth.rpgmap.map.render.MapRegistry;

public class RPGMapManagers {

	// Map cache
	private final MapCache mapCache;
	private final MapRegistry mapRegistry;

	// Storage
	private StorageManager storageManager;

	public RPGMapManagers(RPGMap plugin) {
		this.mapCache = new MapCache();
		this.mapRegistry = new MapRegistry();
		try {
			this.storageManager = StorageType.valueOf(plugin.getConfig().getString("storage").toUpperCase()).get();
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}

	public MapCache getMapCache() {
		return mapCache;
	}

	public MapRegistry getMapRegistry() {
		return mapRegistry;
	}

	public StorageManager getStorageManager() {
		return storageManager;
	}
}
