package net.islandearth.rpgmap.managers.data;

import net.islandearth.rpgmap.managers.data.sql.SqlStorage;
import net.islandearth.rpgmap.managers.data.yml.YamlStorage;

import java.lang.reflect.InvocationTargetException;

public enum StorageType {
	FILE(YamlStorage.class),
	SQL(SqlStorage.class);

	private final Class<? extends StorageManager> clazz;

	StorageType(Class<? extends StorageManager> clazz) {
		this.clazz = clazz;
	}

	public StorageManager get() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		return clazz.getConstructor().newInstance();
	}
}
