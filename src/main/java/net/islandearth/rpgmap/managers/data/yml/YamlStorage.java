package net.islandearth.rpgmap.managers.data.yml;

import com.google.common.collect.ImmutableMap;
import net.islandearth.rpgmap.managers.data.Account;
import net.islandearth.rpgmap.managers.data.StorageManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class YamlStorage implements StorageManager {

	private static final File STORAGE_LOCATION = new File("plugins/RPGMap/storage/");
	private ConcurrentMap<UUID, Account> cachedAccounts = new ConcurrentHashMap<>();

	@Override
	public CompletableFuture<Account> getAccount(UUID uuid) {
		CompletableFuture<Account> future = new CompletableFuture<>();
		if (cachedAccounts.containsKey(uuid)) {
			future.complete(cachedAccounts.get(uuid));
		} else {
			FileConfiguration config = YamlConfiguration.loadConfiguration(new File(STORAGE_LOCATION + "/" + uuid.toString() + ".yml"));
			boolean isMapEnabled = config.getBoolean("isMapEnabled");
			Account account = new Account(uuid, isMapEnabled);
			cachedAccounts.put(uuid, account);
			future.complete(account);
		}
		return future;
	}

	@Override
	public ConcurrentMap<UUID, Account> getCachedAccounts() {
		return (ConcurrentMap<UUID, Account>) ImmutableMap.copyOf(cachedAccounts);
	}

	@Override
	public void removeCachedAccount(UUID uuid) {
		try {
			FileConfiguration config = YamlConfiguration.loadConfiguration(new File(STORAGE_LOCATION + "/" + uuid.toString() + ".yml"));
			config.set("isMapEnabled", cachedAccounts.get(uuid).isMapEnabled());
			config.save(new File(STORAGE_LOCATION + "/" + uuid.toString() + ".yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		cachedAccounts.remove(uuid);
	}
}
