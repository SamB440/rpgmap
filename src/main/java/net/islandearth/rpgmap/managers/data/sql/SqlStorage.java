package net.islandearth.rpgmap.managers.data.sql;

import co.aikar.idb.DB;
import co.aikar.idb.Database;
import co.aikar.idb.DatabaseOptions;
import co.aikar.idb.PooledDatabaseOptions;
import com.google.common.collect.ImmutableMap;
import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.managers.data.Account;
import net.islandearth.rpgmap.managers.data.StorageManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SqlStorage implements StorageManager {

	private ConcurrentMap<UUID, Account> cachedAccounts = new ConcurrentHashMap<>();

	public SqlStorage() {
		RPGMap plugin = JavaPlugin.getPlugin(RPGMap.class);
		DatabaseOptions options = DatabaseOptions.builder().mysql(plugin.getConfig().getString("sql.user"),
				plugin.getConfig().getString("sql.pass"),
				plugin.getConfig().getString("sql.db"),
				plugin.getConfig().getString("sql.host") + ":" + plugin.getConfig().getString("sql.port")).build();
		Database db = PooledDatabaseOptions.builder().options(options).createHikariDatabase();
		DB.setGlobalDatabase(db);
		try {
			db.executeUpdate("CREATE TABLE IF NOT EXISTS Accounts (uuid varchar(32) NOT NULL, map boolean NOT NULL, PRIMARY KEY(uuid))");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public CompletableFuture<Account> getAccount(UUID uuid) {
		CompletableFuture<Account> future = new CompletableFuture<>();
		if (cachedAccounts.containsKey(uuid)) {
			future.complete(cachedAccounts.get(uuid));
		} else {
			try {
				DB.executeInsert("INSERT INTO Accounts (uuid, map) VALUES (?, ?) ON DUPLICATE KEY UPDATE uuid = ?", getDatabaseUuid(uuid), false, getDatabaseUuid(uuid));
				DB.getFirstColumnAsync("SELECT map FROM Accounts WHERE uuid = ?", getDatabaseUuid(uuid)).whenComplete((isMapEnabled, error) -> {
					Account account = new Account(uuid, (boolean) isMapEnabled);
					cachedAccounts.put(uuid, account);
					future.complete(account);
				});
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return future;
	}

	@Override
	public ConcurrentMap<UUID, Account> getCachedAccounts() {
		return (ConcurrentMap<UUID, Account>) ImmutableMap.copyOf(cachedAccounts);
	}

	@Override
	public void removeCachedAccount(UUID uuid) {
		DB.executeUpdateAsync("UPDATE Accounts SET map = ? WHERE uuid = ?", cachedAccounts.get(uuid).isMapEnabled(), getDatabaseUuid(uuid));
		cachedAccounts.remove(uuid);
	}

	private String getDatabaseUuid(UUID uuid) {
		return uuid.toString().replace("-", "");
	}
}
