package net.islandearth.rpgmap.managers.data;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentMap;

public interface StorageManager {

	CompletableFuture<Account> getAccount(UUID uuid);

	ConcurrentMap<UUID, Account> getCachedAccounts();

	void removeCachedAccount(UUID uuid);
}
