package net.islandearth.rpgmap.managers.data;

import java.util.UUID;

public class Account {

	private final UUID uuid;
	private boolean isMapEnabled;

	public Account(UUID uuid, boolean isMapEnabled) {
		this.uuid = uuid;
		this.isMapEnabled = isMapEnabled;
	}

	public UUID getUuid() {
		return uuid;
	}

	public boolean isMapEnabled() {
		return isMapEnabled;
	}

	public void setMapEnabled(boolean isMapEnabled) {
		this.isMapEnabled = isMapEnabled;
	}
}
