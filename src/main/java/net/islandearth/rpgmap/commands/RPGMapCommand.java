package net.islandearth.rpgmap.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import net.islandearth.rpgmap.RPGMap;
import net.islandearth.rpgmap.map.MapLocation;
import net.islandearth.rpgmap.translation.Translations;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCursor;

import java.io.File;
import java.io.IOException;

@CommandAlias("rpgmap")
public class RPGMapCommand extends BaseCommand {

	private final RPGMap plugin;

	public RPGMapCommand(RPGMap plugin) {
		this.plugin = plugin;
	}

	@Default
	@CommandPermission("rpgmap.toggle")
	public void onDefault(Player player, String[] args) {
		plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).whenComplete((account, throwable) -> {
			account.setMapEnabled(!account.isMapEnabled());
			Translations.TOGGLED.send(player);
		});
	}

	@Subcommand("addcursor|createcursor")
	@CommandPermission("rpgmap.addcursor")
	@Syntax("<cursor>")
	@CommandCompletion("@cursors")
	public void addCursor(Player player, String[] args) {
		if (EnumUtils.isValidEnum(MapCursor.Type.class, args[0])) {
			MapCursor.Type type = MapCursor.Type.valueOf(args[0]);
			File file = new File(plugin.getDataFolder() + "/maps/");
			if (!file.exists()) {
				file.mkdirs();
			}

			int x = player.getLocation().getBlockX();
			int y = player.getLocation().getBlockY();
			int z = player.getLocation().getBlockZ();
			File mapFile = new File(plugin.getDataFolder() + "/maps/" + x + "," + y + "," + z + ".yml");
			if (!mapFile.exists()) {
				try {
					mapFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			FileConfiguration config = YamlConfiguration.loadConfiguration(mapFile);
			config.set("x", x);
			config.set("y", y);
			config.set("z", z);
			config.set("world", player.getWorld().getUID().toString());
			config.set("type", type.toString());
			try {
				config.save(mapFile);
				plugin.getManagers().getMapCache().getMapLocations().add(new MapLocation(x, y, z, player.getWorld(), type));
				Translations.LOCATION_SAVED.send(player);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Translations.INVALID_CURSOR.send(player);
		}
	}

	@Subcommand("deletecursor|removecursor")
	@CommandPermission("rpgmap.deletecursor")
	@CommandCompletion("@nothing")
	public void deleteCursor(Player player) {
		int x = player.getLocation().getBlockX();
		int y = player.getLocation().getBlockY();
		int z = player.getLocation().getBlockZ();
		File mapFile = new File(plugin.getDataFolder() + "/maps/" + x + "," + y + "," + z + ".yml");
		if (mapFile.exists()) {
			mapFile.delete();
			MapLocation toRemove = null;
			for (MapLocation mapLocation : plugin.getManagers().getMapCache().getMapLocations()) {
				if (mapLocation.getX() == x && mapLocation.getZ() == z && mapLocation.getY() == y) {
					toRemove = mapLocation;
					break;
				}
			}
			plugin.getManagers().getMapCache().getMapLocations().remove(toRemove);
			Translations.CURSOR_REMOVED.send(player);
		} else {
			Translations.LOCATION_NOT_SAVED.send(player);
		}
	}
}
